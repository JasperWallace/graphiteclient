#!/usr/bin/env python3
#
#
#

import sys, time, subprocess, traceback
from socket import socket, gethostname

class GraphiteClient:
    def __init__(self, server="bogwoppit.lan.pointless.net", port=2003, delay=60):
        self.port = port
        self.server = server
        # when dryrun is true we don't send anything, just print it.
        self.dryrun = False
        # when verbose is true we print what we send
        self.verbose = True
        self.fail = False
        self.hostname = gethostname()
        self.delay = delay
        self.connect()

    def connect(self):
        self.sock = socket()
        try:
            if not self.dryrun:
                self.sock.connect( (self.server, self.port) )
            self.fail = False
        except:
            print(traceback.format_exc())
            print("Couldn't connect to %(server)s on port %(port)d, is carbon-agent.py running?" % { 'server': self.server, 'port': self.port })
            self.fail = True

    # loop forever calling a single callback
    # key is e.g. "mything.%s.something"
    # must have %s, will be replaced with the hostname
    # callback is a callable that returns an int or string
    def run(self, key, callback):
        while True:
            value = callback()
            self.poke(key, value)
            time.sleep(self.delay)

    # have values poked at us from somewhere.
    def poke(self, key, value):
        lines = []

        # e.g. "mything.%s.something" to add the hostname to it.
        if '%s' in key:
            mess = key % (self.hostname)
        else:
            # no hostname in the key
            mess = key

        if type(value) == type(42):
            mess += " %d %d" % (value, int(time.time()) )
        else:
            mess += " %s %d" % (str(value), int(time.time()) )
        lines.append(mess)
        message = '\n'.join(lines) + '\n' #all lines must end in a newline
        self.send(message)

    def send(self, message):
        if self.fail:
            self.connect()
        try:
            if not self.dryrun:
                self.sock.sendall(message.encode("utf-8"))
            if self.dryrun or self.verbose:
                print(message)
        except Exception as e:
            print(traceback.format_exc())
            print(e)
            self.fail = True
            self.connect()
        sys.stdout.flush()
