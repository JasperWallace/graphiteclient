#!/usr/bin/env python
#
from distutils.core import setup
import sys, os, re, shelve, urllib

#
VERSION = "0.2.1"

if __name__ == "__main__":
  setup(
    name = 'graphiteclient',
    version = VERSION,
    author = 'Jasper Wallace',
    author_email = 'jasper@pointless.net',
    url = 'http://pointless.net/',
    description = 'Simple Graphite client',
    long_description = '''
graphiteclient
--------------

Poke data into graphite

    ''',
    license = 'BSD',
    py_modules = ['graphiteclient'],
    classifiers = [
    'Development Status :: 4 - Beta',
    'Environment :: Console',
    'Intended Audience :: System Administrators',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    ]
  )
